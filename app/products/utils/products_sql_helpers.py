from app.products.models import Product, ProductPrice,  AvailableDate
from sqlalchemy.sql import func, desc, column, select, and_
from sqlalchemy import cast
from sqlalchemy.types import Interval

def price_group_cte(db_session, from_date, to_date, filter=False):

    to_date_price = db_session.query(
        ProductPrice.product_id,
        ProductPrice.price,
        ProductPrice.date,
    ).filter(ProductPrice.date == to_date).subquery()

    from_date_price = db_session.query(
        ProductPrice.product_id,
        ProductPrice.price,
    ).filter(ProductPrice.date == from_date).subquery()

    price_group = db_session.query(
        to_date_price.c.product_id.label('product_id'),
        to_date_price.c.price.label('last_price'),
        func.coalesce(from_date_price.c.price, 0).label('first_price'),
        to_date_price.c.date.label('last_date')
    ) \
        .outerjoin(from_date_price, to_date_price.c.product_id == from_date_price.c.product_id)

    if filter:
        price_group = price_group.filter(to_date_price.c.price < from_date_price.c.price)
    return price_group.cte('price_group')


def min_max_prices(db_session, from_date, to_date, displayed_products, num_col):

    column_ranges = ','.join([str(c) for c in range(1, num_col+1)])
    return db_session.query(
        displayed_products,
        func.min(ProductPrice.price).label('min_price'),
        func.max(ProductPrice.price).label('max_price'),
    )\
        .join(ProductPrice, displayed_products.c.id == ProductPrice.product_id)\
        .filter(ProductPrice.date.between(from_date, to_date))\
        .group_by('{}'.format(column_ranges))

def get_available_date():

    date = AvailableDate.query.order_by(desc(AvailableDate.date)).first()
    return date.date


def get_date_ranges(date_from, date_to):

    date_series = select([column('generate_series').label('date')])\
        .select_from(
            func.generate_series(
                date_from,
                date_to,
                cast('1 day', Interval)
            )
    ).cte('date_series')

    return date_series


def product_filter(product_query, price_group, distributor_ids=[], price_from=0, price_to=0):

    if len(distributor_ids) > 0:
        product_query = product_query.filter(Product.distributor_id.in_(distributor_ids))

    if price_from != 0:
        product_query = product_query.filter(price_group.c.last_price >= price_from)

    if price_to != 0:
        product_query = product_query.filter(price_group.c.last_price <= price_to)

    return product_query