# Import flask dependencies
from flask import Blueprint, abort, request, make_response, jsonify, url_for
from flask.ext.cors import cross_origin

from datetime import datetime
# Import the database object from the main app module
from app import db

# Import module models
from app.users.models import User, WatchPrice
from app.products.models import Product, ProductPrice, Distributor
from app.products.utils.products_sql_helpers import get_available_date
from sqlalchemy.sql import func, and_

# Define the blueprint: 'user', set its url prefix: app.url/user
mod = Blueprint('users', '__users__', url_prefix='/users')


@cross_origin()
@mod.route('/token', methods=['POST'])
def auth_token():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)
    return jsonify({
        'social_id': user.social_id,
        'first_name': user.first_name,
        'token': token.decode('ascii')
    })

@cross_origin()
@mod.route('/email-settings', methods=['POST'])
def email_settings():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)
    return jsonify({
        'email': user.email,
        'email_newsletter': user.email_newsletter,
        'email_alert': user.email_alert
    })

@cross_origin()
@mod.route('/update-email-settings', methods=['POST'])
def update_email_settings():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)

    user.email_newsletter = request.args.get('email_newsletter')
    user.email_alert = request.args.get('email_alert')
    db.session.commit()
    return make_response(jsonify( { 'message': 'Update email settings successfully' } ), 200)

# Get list of watched products
@cross_origin()
@mod.route('/watched-products', methods=['POST'])
def watch_price():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)

    # Get list of product with watched price and current price
    latest_date = get_available_date()
    product_query = db.session.query(
        Product.id.label('product_id'), Product.name, Product.brand, Product.model, Product.image,
        Distributor.name.label('distributor'),
        ProductPrice.price.label('current_price'),
        WatchPrice.price.label('desired_price'),
        WatchPrice.started_time.label('started_time')
    ). \
    join(WatchPrice, and_(WatchPrice.product_id == Product.id, WatchPrice.user_id == user.id)).\
    join(Distributor, Distributor.id == Product.distributor_id). \
    join(ProductPrice, and_(ProductPrice.product_id == Product.id, ProductPrice.date == latest_date))

    data = product_query

    return jsonify(products=[dict(zip(row.keys(), row)) for row in data])


# Get watched price of a specified product
@cross_origin()
@mod.route('/watched-product', methods=['POST'])
def watched_product():
    token = request.args.get('token')
    if token:
        user = User.verify_auth_token(token)
        if user:
            product_id = request.args.get('product_id')
            watch = WatchPrice.query.filter_by(user_id=user.id, product_id=product_id).first()
            if watch:
                return jsonify(price=watch.price, watched=True)

    return jsonify(price=None, watched=False)

# Watch new product
@cross_origin()
@mod.route('/watch-product', methods=['POST'])
def watch_product():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)

    product_id = request.args.get('product_id')

    # Check if there is already a watch for this product
    watch = WatchPrice.query.filter_by(user_id=user.id, product_id=product_id).first()
    price = request.args.get('price', default=None)

    if watch:
        watch.price = price
    else:
        watch = WatchPrice(user_id=user.id, product_id=product_id, price=price, started_time=datetime.now())
        db.session.add(watch)

    db.session.commit()

    return jsonify({'message': 'successful'})

@cross_origin()
@mod.route('/update-watched-price', methods=['POST'])
def update_watched_price():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)

    product_id = request.args.get('product_id')
    watch = WatchPrice.query.filter_by(user_id=user.id, product_id=product_id).first()

    if watch:
        desired_price = request.args.get('desired_price', type=float)
        if desired_price > 0:
            watch.price = desired_price
            watch.started_time = datetime.now()
        else:
            watch.price = None
        db.session.commit()
        return jsonify({'desired_price': desired_price})

    return make_response(jsonify( { 'message': 'Watch not found' } ), 404)

@cross_origin()
@mod.route('/unwatch', methods=['POST'])
def unwatch():
    token = request.args.get('token')
    if not token:
        return make_response(jsonify( { 'message': 'No token' } ), 400)
    user = User.verify_auth_token(token)
    if not user:
        return make_response(jsonify( { 'message': 'Invalid token' } ), 400)

    product_id = request.args.get('product_id')
    watch = WatchPrice.query.filter_by(user_id=user.id, product_id=product_id).first()

    if watch:
        db.session.delete(watch)
        db.session.commit()
        return make_response(jsonify( { 'message': 'Unwatched successfully' } ), 200)

    return make_response(jsonify( { 'message': 'Watch not found' } ), 404)
