__author__ = 'steveph'
from flask import request, current_app
import json
import hashlib
import struct
from functools import wraps
from app.products.models import Cache
from products_sql_helpers import get_available_date
from app import db
import os

def product_cache(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        request_id = hash_request_argument()
        cache_factory = CacheFactory()
        distributor_ids = request.args.getlist('distributor_ids')
        data = cache_factory.get(request_id, distributor_ids)
        if data is None:
            response = f(*args, **kwargs)
            cache_object = Cache(request_id, response, distributor_ids)
            cache_factory.save(cache_object)
        else:
            response = data
        return current_app.response_class(response, mimetype='application/json')
    return wrapped


def hash_request_argument():
    request_args = dict(request.args)
    current_date = get_available_date()
    if 'token' in request_args:
        request_args.pop('token')
    request_args['path'] = request.path
    request_args['date'] = current_date.strftime('%Y-%m-%d')

    request_id = struct.unpack('>q', hashlib.md5(json.dumps(request_args)).digest()[0:8])[0]
    return request_id


class CacheFactory:

    def __init__(self):
        self.db_session = db.session
        self.__cache_location__ = current_app.config['CACHE_LOCATION']
        self.__cache_option__ = current_app.config['CACHE_OPTION']

    def get(self, request_id, distributor_ids):
        if self.__cache_option__ == 'file':
            json_file = self.get_file_path(request_id, distributor_ids)
            if os.path.isfile(json_file):
                return json.load(open(json_file))
        else:
            return self.db_session.query.get(request_id).response
        return None

    def save(self, cache_object):
        if self.__cache_option__ == 'file':
            if len(cache_object.distributors) == 0:
                dir_name = 'dist|all'
            else:
                dir_name = 'dist|{}'.format('_'.join(cache_object.distributors), '|')

            dir_name = '{}/{}'.format(self.__cache_location__, dir_name)
            if not os.path.exists(dir_name):
                os.mkdir(dir_name, 0755)

            json_file = '{}/cache_{}'.format(dir_name, cache_object.id)
            json.dump(cache_object.response, open(json_file, 'w'))
        else:
            self.db_session.add(cache_object)
            self.db_session.commit()

    def get_file_path(self, request_id, distributor_ids):
        if len(distributor_ids) == 0:
            dir_name = 'dist|all'
        else:
            dir_name = 'dist|{}'.format('_'.join(distributor_ids),'|')

        dir_name = '{}/{}'.format(self.__cache_location__, dir_name)
        json_file = '{}/cache_{}'.format(dir_name, request_id)

        return json_file

