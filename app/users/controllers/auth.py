# Import flask dependencies
from flask import Blueprint, abort, request, make_response, jsonify, url_for
from flask.ext.cors import cross_origin

from datetime import datetime
# Import the database object from the main app module
from app import db

# Import module models
from app.users.models import User

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod = Blueprint('auth', '__auth__', url_prefix='/auth')

# Connect to Facebook
@cross_origin()
@mod.route('/connectfb', methods=['POST'])
def connect_fb():
    email = request.args.get('email')
    if not email or email is None:
        abort(404)
    user = User.query.filter_by(email=email).first()
    if not user:
        status = 1 # Facebook user is auto-verified
        social_platform = 'facebook'
        social_id = request.args.get('id')
        email = request.args.get('email')
        name = request.args.get('name')
        first_name = request.args.get('first_name')
        last_name = request.args.get('last_name')
        gender = request.args.get('gender')
        joined_time = datetime.now()

        user = User(
            status=status,
            social_platform=social_platform,
            social_id=social_id,
            email=email,
            name=name,
            first_name=first_name,
            last_name=last_name,
            gender=gender,
            joined_time=joined_time
        )

        db.session.add(user)
        db.session.commit()

    token = user.generate_auth_token()
    return jsonify({
        'token': token.decode('ascii')
    })

