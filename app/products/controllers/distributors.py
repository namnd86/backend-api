from flask import jsonify, abort, Blueprint
from app.products.models import Distributor

mod = Blueprint('distributors', '__distributor__', url_prefix='/distributors')
@mod.route('/')
@mod.route('/<int:distributor_id>')
def get(distributor_id=None):
    if distributor_id is None:
        distributors = Distributor.query.all()

        return jsonify(distributors=[d.serialize() for d in distributors])

    else:
        distributor = Distributor.query.get(distributor_id)
        if not distributor:
            abort(404)
        return jsonify(distributor.serialize())

