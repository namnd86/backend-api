from app import db
from sqlalchemy.schema import ForeignKey, PrimaryKeyConstraint, Index
from sqlalchemy import Sequence
from sqlalchemy import Column, Integer, String, Text, Date, Float, BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import TSVECTOR,ARRAY, JSONB
from flask import current_app
import os, app, json


class Product(db.Model):
    __tablename__ = 'products'

    id = Column(Integer, Sequence('product_id_seq'), primary_key=True)
    distributor_id = Column(Integer, ForeignKey("distributors.id"))
    name = Column(Text())
    model = Column(Text(), nullable=False)
    brand = Column(Text())
    url = Column(Text(), nullable=False)
    image = Column(Text())
    description = Column(Text())
    text_search = Column(TSVECTOR)

    product_prices = relationship("ProductPrice")
    index = Index('product_text_index', text_search, postgresql_using='gist')


    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'model': self.model,
            'brand': self.brand,
            'url': self.url,
            'image': self.image
        }


class ProductPrice(db.Model):
    __tablename__ = 'product_prices'
    __table_args__ = (
        PrimaryKeyConstraint('product_id', 'date'),
    )

    product_id = Column(Integer, ForeignKey("products.id"))
    date = Column(Date, nullable=False)
    price = Column(Float, nullable=False)
    index = Index('product_prices_date_idx', date, postgresql_using='btree')

    def serialize(self):
        return {
            'date': str(self.date),
            'price': self.price
        }


class Distributor(db.Model):
    __tablename__ = 'distributors'

    id = Column(Integer, Sequence('distributor_id_seq'), primary_key=True)
    name = Column(String(50))

    products = relationship("Product")

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name
        }

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<id {}>' . format(self.id)


class AvailableDate(db.Model):
    __tablename__ = 'available_dates'

    date = Column(Date, primary_key=True)

class Cache(db.Model):
    __tablename__ = 'cache'


    id = Column(BigInteger, primary_key=True)
    response = Column(JSONB)
    distributors = Column(ARRAY(Text, dimensions=1))

    def __init__(self, id, response, distributors=[]):
        self.id = id
        self.response = response
        self.distributors = distributors

   