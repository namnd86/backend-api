__author__ = 'steveph'

PER_PAGE = 20
SIMILARITY = 0.05
SIMILAR_PRODUCTS_LIMIT = 5
NUM_DAYS_FROM_ONE_WEEK = 7
NUM_DAYS_FROM_YESTERDAY = 2
QUICKSEARCH_NUM_ITEMS = 5
SEARCH_TYPES = [
    'similarity desc',
    'current_price asc',
    'current_price desc',
    'relative_diff desc',
    'relative_diff asc',
    'absolute_diff desc',
    'absolute_diff asc',
]
