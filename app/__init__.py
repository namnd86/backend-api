from flask import Flask

from flask.ext.sqlalchemy import SQLAlchemy
from flask import make_response, jsonify

# Import CORS
from flask.ext.cors import CORS
import socket
import os

app = Flask(__name__)
hostname = socket.gethostname()
if hostname == 'ip-172-31-8-72':
    APP_SETTINGS="config.ProductionConfig"
else:
    APP_SETTINGS="config.DevelopmentConfig"

app.config.from_object(APP_SETTINGS)
db = SQLAlchemy(app)
if app.config.get('CACHE_LOCATION'):
    if not os.path.exists(app.config['CACHE_LOCATION']):
        os.mkdir(app.config['CACHE_LOCATION'], 0755)

# Extensions
cors = CORS(app)

from app.products.controllers.products import mod as products_module
from app.products.controllers.distributors import mod as distributors_module

from app.users.controllers.auth import mod as auth_module
from app.users.controllers.users import mod as users_module

app.register_blueprint(distributors_module)
app.register_blueprint(products_module)
app.register_blueprint(auth_module)
app.register_blueprint(users_module)

@app.route('/')
def index():
    """Return a friendly HTTP greeting."""
    return 'OZ Price Tracker API'

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return make_response(jsonify( { 'message': 'Bad request' } ), 400)


@app.errorhandler(401)
def unauthorized_access(e):
    return make_response(jsonify( { 'message': 'Unauthorized access' } ), 400)
