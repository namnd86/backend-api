class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'awesome-price-tracking-app'
class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "postgresql://awsadmin:0zpptr4ck3rDB@pricetracker.cfuk43phr5q8.ap-southeast-2.rds.amazonaws.com:5432/pricetrackor"
    CACHE_LOCATION = '/var/www/api.ozpricetracker.com/cache'
    CACHE_OPTION = 'file'

class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "postgresql://awsadmin:0zpptr4ck3rDB@pricetracker.cfuk43phr5q8.ap-southeast-2.rds.amazonaws.com:5432/pricetrackor"
    CACHE_LOCATION = './cache'
    CACHE_OPTION = 'file'
    SQLALCHEMY_ECHO = True

class TestingConfig(Config):
    TESTING = True
