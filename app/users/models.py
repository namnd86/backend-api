from app import db
from sqlalchemy.schema import ForeignKey, PrimaryKeyConstraint, Index
from sqlalchemy import Sequence
from sqlalchemy import Column, Integer, String, Text, Date, Float, DateTime, Boolean

# Import hashing algorithm
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)

from app.users import constants as USER_CONSTANTS

# User model
class User(db.Model):
    __tablename__ = 'users'
    id = Column(db.Integer, primary_key=True)
    status = Column(db.Integer, default=0)
    social_platform = Column(String(50))
    social_id = Column(String(50))
    email = Column(String(255), index=True)
    password_hash = Column(String(255))
    name = Column(String(100))
    first_name = Column(String(50))
    last_name = Column(String(50))
    gender = Column(String(10))
    email_newsletter = Column(Boolean, default=False)
    email_alert = Column(Boolean, default=True)
    joined_time = Column(DateTime)

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=60000):
        s = Serializer(USER_CONSTANTS.SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(USER_CONSTANTS.SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

# Watch product model
class WatchPrice(db.Model):

    __tablename__ = 'watch_prices'
    __table_args__ = (
        PrimaryKeyConstraint('user_id', 'product_id'),
    )

    user_id = Column(Integer, ForeignKey("users.id"))
    product_id = Column(Integer, ForeignKey("products.id"))
    price = Column(Float, nullable=True)
    started_time = Column(DateTime, nullable=False)




