from datetime import timedelta, date
import math
from app import db
from flask.ext.cors import cross_origin
from app.products.models import Product, ProductPrice, Distributor
from sqlalchemy.sql import func, desc, and_
import sqlalchemy.sql
from flask import request, Blueprint, abort
from app.products import constants as PRODUCT_CONSTANTS
from app.products.utils.products_sql_helpers import price_group_cte, min_max_prices, get_available_date, get_date_ranges, product_filter
from sqlalchemy import cast, case
from sqlalchemy.types import Date
from app.products.utils.cache_helpers import product_cache
import json
import re

mod = Blueprint('products', '__products__', url_prefix='/products')


@cross_origin
@mod.route('/top-drop', methods=('GET', 'POST'), )
@product_cache
def top_drop():

    interval = request.args.get('interval', default=PRODUCT_CONSTANTS.NUM_DAYS_FROM_YESTERDAY, type=int)
    page = request.args.get('page', default=0, type=int)
    sort_type = request.args.get('sort_type', default='relative_diff asc')
    distributor_ids = request.args.getlist('distributor_ids')
    price_from = request.args.get('price_from', default=0, type=int)
    price_to = request.args.get('price_to', default=0, type=int)
    items_per_page = request.args.get('items_per_page', default=PRODUCT_CONSTANTS.PER_PAGE, type=int)

    if sort_type not in ['relative_diff asc', 'absolute_diff asc', 'current_price asc', 'current_price desc']:
        abort(404)

    sort_type = sort_type.split(' ')
    order = getattr(sqlalchemy.sql, sort_type[1])

    current_date = get_available_date()
    from_date = current_date - timedelta(days=interval - 1)

    price_group = price_group_cte(db.session, from_date, current_date, filter=True)

    product_query = db.session.query(
        Product.id, Product.name, Product.url, Product.model, Product.brand,
        Product.image, Distributor.name.label('distributor'),
        price_group.c.last_price.label('current_price'),
        (price_group.c.last_price - price_group.c.first_price).label('absolute_diff'),
        ((price_group.c.last_price - price_group.c.first_price) / price_group.c.first_price).label('relative_diff')
    ) \
        .join(price_group, Product.id == price_group.c.product_id) \
        .join(Distributor) \
        .filter(price_group.c.last_price > 0) \
        .order_by(order(sort_type[0]))

    product_query = product_filter(product_query, price_group, distributor_ids, price_from, price_to)
    total = product_query.count()
    displayed_products = product_query.limit(items_per_page).offset(page * items_per_page).subquery(name='products')

    data = min_max_prices(db.session, from_date, current_date, displayed_products, 10)
    data = data.order_by(order(sort_type[0]))

    json_data = dict(
        products=[dict(zip(row.keys(), row)) for row in data],
        num_pages=math.ceil(float(total) / items_per_page),
        total_items=total
    )

    return json.dumps(json_data)


@mod.route('/search', methods=('GET', 'POST'))
@product_cache
def search():

    query = request.args.get('keyword')
    interval = request.args.get('interval', default=PRODUCT_CONSTANTS.NUM_DAYS_FROM_YESTERDAY, type=int)
    search_type = request.args.get('sort_type', default="similarity desc")
    page = request.args.get('page', default=0, type=int)
    distributor_ids = request.args.getlist('distributor_ids')
    price_from = request.args.get('price_from', default=0, type=int)
    price_to = request.args.get('price_to', default=0, type=int)
    items_per_page = request.args.get('items_per_page', default=PRODUCT_CONSTANTS.PER_PAGE, type=int)

    if not search_type in PRODUCT_CONSTANTS.SEARCH_TYPES:
        search_type = 'similarity desc'
    search_type = search_type.split(' ')
    order = getattr(sqlalchemy.sql, search_type[1])

    current_date = get_available_date()
    from_date = current_date - timedelta(days=interval-1)

    price_group = price_group_cte(db.session, from_date, current_date)

    product_query = db.session.query(
        Product.id, Product.name, Product.url, Product.model, Product.brand,
        Product.image, Distributor.name.label('distributor'),
        price_group.c.last_price.label('current_price'),
        case(
            [(price_group.c.first_price == 0, 0)],
            else_=(price_group.c.last_price - price_group.c.first_price)
        ).label('absolute_diff'),
        case(
            [(price_group.c.first_price == 0, 0)],
            else_=(price_group.c.last_price - price_group.c.first_price)/price_group.c.first_price
        ).label('relative_diff'),
        func.similarity(
            Product.name,
            query
        ).label('similarity')
    ) \
        .join(price_group, Product.id == price_group.c.product_id) \
        .join(Distributor) \
        .filter(
            Product.text_search.op('@@')(func.plainto_tsquery('english', query))
        ) \
        .filter(
            price_group.c.last_price > 0
        ) \
        .order_by(order(search_type[0]))

    product_query = product_filter(product_query, price_group, distributor_ids, price_from, price_to)
    total = product_query.count()
    displayed_products = product_query.limit(items_per_page).offset(page * items_per_page).subquery(name='products')

    data = min_max_prices(db.session, from_date, current_date, displayed_products, 11)
    data = data.order_by(order(search_type[0]))

    json_data = dict(
        products=[dict(zip(row.keys(), row)) for row in data],
        num_pages=math.ceil(float(total) / items_per_page),
        total_items=total
    )

    return json.dumps(json_data)


@mod.route('/quicksearch/<query>')
def quick_search(query=None):
    products = []
    query = query.strip()
    if len(query) >= 3:
        query = re.sub(r'[^\w\s]', '', query)
        query = '%'+re.sub(r'\s+', '%', query)+'%'
        products = db.session.query(
            Product.id, Product.name, Product.url, Product.model,
            Product.image, Distributor.name.label('distributor')
        ) \
            .join(Distributor) \
            .filter(
                Product.name.ilike(query)
            ) \
            .order_by(desc(func.similarity(Product.name, query))) \
            .limit(PRODUCT_CONSTANTS.QUICKSEARCH_NUM_ITEMS).all()
    json_data = dict(
        products=[dict(zip(row.keys(), row)) for row in products]
    )

    return json.dumps(json_data)


@mod.route('/similar-products/<int:product_id>', methods=('GET', 'POST'))
@product_cache
def similar_products(product_id):
    if product_id is None:
        abort(400) # Bad request

    current_product = Product.query.get(product_id)

    if not current_product:
        abort(404) # Product not found

    distributor_ids = request.args.getlist('distributor_ids')

    current_date = get_available_date()

    product = db.session.query(
        Product,
        func.row_number().over(
            partition_by= Product.distributor_id,
            order_by=desc(
                func.similarity(
                    Product.name,
                    current_product.name
                )
            )
        ).label('row_number'),
        func.similarity(
            Product.name,
            current_product.name
        ).label('similarity')

    )\
        .filter(
            func.similarity(
                Product.name,
                current_product.name
            ) > PRODUCT_CONSTANTS.SIMILARITY
        )\
        .filter(Product.id != current_product.id)\
        .cte(name='similar_product')

    relevant_products = db.session.query(
        product.c.id.label('id'), product.c.name.label('name'), product.c.brand.label('brand'),
        product.c.image.label('image'), product.c.model.label('model'), product.c.url.label('url'),
        Distributor.name.label('distributor'),
        ProductPrice.price.label('price')
    )\
        .join(Distributor, Distributor.id == product.c.distributor_id) \
        .join(ProductPrice, and_(ProductPrice.product_id == product.c.id, ProductPrice.date == current_date)) \
        .filter(product.c.row_number <= PRODUCT_CONSTANTS.SIMILAR_PRODUCTS_LIMIT) \
        .order_by(desc(product.c.similarity))
    
    if len(distributor_ids) > 0:
        relevant_products = relevant_products.filter(product.c.distributor_id.in_(distributor_ids))

    values = [
        dict(zip(p.keys(), p))
        for p in relevant_products
    ]

    json_data = dict(similar_products=values, current_product=current_product.serialize())

    return json.dumps(json_data)


@mod.route('/<int:product_id>')
@product_cache
def get(product_id):
    if product_id is None:
        abort(400) # Bad request

    product = Product.query.get(product_id)
    if not product:
        abort(404) # Product not found
    interval = request.args.get('interval', default=PRODUCT_CONSTANTS.NUM_DAYS_FROM_ONE_WEEK, type=int)
    current_date = get_available_date()
    from_date = current_date - timedelta(days=interval-1)

    date_series = get_date_ranges(from_date, current_date)

    prices = db.session.query(
        func.DATE(date_series.c.date).label('date'),
        func.coalesce(ProductPrice.price, 0).label('price')
    ) \
        .outerjoin(ProductPrice, and_(
            ProductPrice.date == cast(date_series.c.date, Date),
            ProductPrice.product_id == product_id)) \
        .order_by('date')

    distributor = Distributor.query.get(product.distributor_id)

    product_prices = []
    for price in prices:
        p = {}
        for key, value in zip(price.keys(), price):
            v = value
            if isinstance(value, date):
                v = value.strftime('%Y-%m-%d')
            p[key] = v


        product_prices.append(p)

    if product_prices[-1]['price'] == 0:
        product_prices[-1]['price'] = product_prices[-2]['price']

    json_data = dict(
        product=product.serialize(),
        prices= product_prices,
        distributor=distributor.serialize()
    )

    return json.dumps(json_data)
